#!/bin/sh

if ! [ -x "$(command -v cargo)" ]; then
  echo 'Error: Rust is not installed.  To install Rust, get rustup from https://rustup.rs/ and install the nightly version.';
  exit 1;
fi

if ! [ -x "$(command -v diesel)" ]; then
  echo 'Error: diesel_cli is not installed.  To install it, run `cargo install diesel_cli --features=sqlite`';
  exit 1;
fi

echo "CURRENT DIR::::::: $(pwd)"

export DATABASE_URL='db.sqlite'
# Sets up the sqlte database
diesel setup
diesel migration run

# symlink the database to the src directory so that it's detected by the Rust Enhanced sublime plugin
if ! [ -L "src/db.sqlite" ]; then
  ln -s $(pwd)/db.sqlite src/db.sqlite
fi

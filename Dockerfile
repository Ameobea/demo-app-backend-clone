FROM mackeyja92/rustup:nightly

MAINTAINER Casey Primozic, Clayton Marshall

RUN apt-get update

# Add required dependencies
RUN apt-get install sqlite3 libsqlite3-dev libpq-dev libmysqlclient-dev -y

# Install the diesel CLI with sqlite3 support
RUN cargo install diesel_cli --features=sqlite

# Move source code and scripts from local filesystem into the image
COPY src /app/src
COPY Cargo.toml /app/Cargo.toml
COPY setup.sh /app/setup.sh
COPY migrations /app/migrations

WORKDIR /app

# Run the setup script to create the database and run initial migrations to create the schema
RUN ./setup.sh

# Expose the port that the application starts on
EXPOSE 8000

ENV ROCKET_ENV="production"

# Compile and initialize the application
CMD ["cargo", "run", "--release"]

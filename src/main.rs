//! Demo Application Backend
//!
//! Simple CRUD wrapper around a database for the demo loan application frontend.

#![feature(try_trait, plugin)]
#![plugin(rocket_codegen)]
#![allow(non_snake_case)]

#[macro_use]
extern crate diesel_codegen;
#[macro_use]
extern crate diesel;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate rocket;

extern crate rocket_contrib;
use rocket_contrib::JSON;

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use std::fmt::Debug;
use std::ops::Try;

use diesel::sqlite::SqliteConnection;
use rocket::http::Status;
use rocket::request::Request;
use rocket::response::{Responder, Response};

mod db_interface;
use db_interface::{create_db_pool, DbPool};
mod models;
use models::{User, NewUser};
mod routes;
use routes::*;
mod schema;
use schema::users;

// helper function for converting a debuggable object into a `String`
fn debug<T: Debug>(a: T) -> String { format!("{:?}", a) }

/// The main response returned from successful API requests.  Can be extended or made generic to accommodate future development.
#[derive(Serialize, Deserialize)]
pub struct ApiSuccess {
    message: String,
}

/// The main response returned from unsuccessful API requests resulting from bad use input.  Can be extended or made generic to
/// accommodate future development.
#[derive(Serialize, Deserialize)]
pub struct ApiError {
    message: String,
}

impl ApiError {
    pub fn new<T: Into<String>>(msg: T) -> Self {
        ApiError {message: msg.into()}
    }
}

/// This is the response type that is returned by all API endpoints.
pub enum ApiResult {
    Success(ApiSuccess),
    Error(ApiError),
}

impl ApiResult {
    fn success(message: String) -> Self {
        ApiResult::Success(ApiSuccess{message})
    }
}

// This allows the `?` operator to be used with `ApiResult` structs.  If the request is successful, it returns a code
// 200.  When the input is invalid, it returns a 400.  If the requested endpoint is invalid or doesn't exist, it returns
// a 404.  If an unhanded error occurs, it returns a 500.
impl Try for ApiResult {
    type Ok = ApiSuccess;
    type Error = ApiError;

    fn into_result(self) -> Result<ApiSuccess, ApiError> {
        match self {
            ApiResult::Success(success) => Ok(success),
            ApiResult::Error(err) => Err(err),
        }
    }

    fn from_error(err: ApiError) -> Self {
        ApiResult::Error(err)
    }

    fn from_ok(success: ApiSuccess) -> Self {
        ApiResult::Success(success)
    }
}

// This implementation defines a method to use `ApiResult`s as responders to requests.  By this manner, we can automatically
// convert errors to 400s containing a message and results to 200s.
impl Responder<'static> for ApiResult {
    fn respond_to(self, req: &Request) -> Result<Response<'static>, Status> {
        Ok(match self {
            ApiResult::Success(success) => JSON(success).respond_to(req).unwrap(),
            ApiResult::Error(err) => {
                let mut res = JSON(err).respond_to(req).unwrap();
                res.set_status(Status::BadRequest);
                res
            },
        })
    }
}

#[derive(Serialize, Deserialize)]
struct ErrorMessage {
    status: u16,
    message: String,
}

/// This function matches all routes that aren't defined, returning a 404 error.
#[error(404)]
fn not_found(_: &Request) -> JSON<ErrorMessage> {
    JSON(ErrorMessage {
        status: 404,
        message: "The requested resource could not be found.".into(),
    })
}

/// This function is called every time there's an internal error while attempting to handle a request.
#[error(500)]
fn internal_error(_: &Request) -> JSON<ErrorMessage> {
    JSON(ErrorMessage {
        status: 500,
        message: "An internal server error occurred and we were unable to process your request.".into(),
    })
}

fn main() {
    rocket::ignite()
        .mount("/", routes![hello, new_user, grab_user])
        .catch(errors![not_found, internal_error])
        .manage(DbPool(create_db_pool()))
        .launch();
}

#[test]
fn insert_select_user() {
    use diesel::prelude::*;
    use schema::users::dsl as users_dsl;

    let conn = SqliteConnection::establish("db.sqlite").expect("Unable to create connection to Sqlite database!");

    let new_user = NewUser {
        firstName: String::from("John"),
        middleInitial: Some(String::from("Z")),
        lastName: String::from("Cena"),
        soc: 328283372,
        dob: String::from("04/20/1999"),
    };

    diesel::insert(&new_user)
        .into(users::table)
        .execute(&conn)
        .expect("Error inserting user into database!");

    let result: Vec<User> = users_dsl::users
                        .filter(users_dsl::firstName.eq(String::from("John")))
                        .limit(1)
                        .order(users_dsl::id.desc())
                        .load(&conn)
                        .expect("Error John wasn't here.");

    assert_eq!(new_user, result[0].clone().into());
}

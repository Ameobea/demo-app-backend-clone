//! Functions for interfacing with the database backend.  Contains functions for both inserting and querying data.

use diesel;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use r2d2::{Pool, PooledConnection, Config};
use r2d2_diesel::ConnectionManager;

use schema::users;
use super::{ApiError, NewUser};

/// A wrapper struct around an inner connection pool.  Provides a helper function for pulling a connection out of the pool.
pub struct DbPool(pub Pool<ConnectionManager<SqliteConnection>>);

impl DbPool {
    pub fn get_conn(&self) -> PooledConnection<ConnectionManager<SqliteConnection>> {
        return self.0.get().unwrap()
    }
}

pub fn insert_new_user(user: &NewUser, conn: &SqliteConnection) -> Result<usize, ApiError> {
    diesel::insert(user)
        .into(users::table)
        .execute(conn)
        .map_err(|err| ApiError::new(format!("An error occurred while attempting to insert user into the database: {:?}", err)))
}

pub fn create_db_pool() -> Pool<ConnectionManager<SqliteConnection>> {
    let config = Config::default();
    let manager = ConnectionManager::<SqliteConnection>::new("db.sqlite");
    Pool::new(config, manager).expect("Failed to create pool.")
}

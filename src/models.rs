//! Definitions of the database schema in terms of Rust objects.
use schema::users;

#[derive(Clone, Debug, PartialEq, Queryable, Serialize, Deserialize)]

pub struct User {
    pub id: i32,
    pub firstName: String,
    pub middleInitial: Option<String>,
    pub lastName: String,
    pub soc: i32,
    pub dob: String
}

#[derive(Clone, Debug, Insertable, PartialEq, Serialize, Deserialize)]
#[table_name="users"]
pub struct NewUser {
    pub firstName: String,
    pub middleInitial: Option<String>,
    pub lastName: String,
    pub soc: i32,
    pub dob: String
}

impl Into<NewUser> for User {
    fn into(self) -> NewUser {
        NewUser {
            firstName: self.firstName,
            middleInitial: self.middleInitial,
            lastName: self.lastName,
            soc: self.soc,
            dob: self.dob,
        }
    }
}

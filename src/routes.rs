// Definitions of the actual API routes that are used with Rocket.

use diesel::prelude::*;
use rocket::State;

use rocket_contrib::JSON;
use serde::Deserialize;
use serde_json;

use db_interface::{DbPool, insert_new_user};
use models::{User, NewUser};
use super::{ApiError, ApiResult};

/// Given a `String`, attempts to convert it to its native struct representation.  In the event of failure,
/// returns `ApiResult::Error(_)` detailing the error.
fn unwrap_json<T: for<'de> Deserialize<'de>>(s: &str) -> Result<T, ApiError> {
    serde_json::from_str(s).map_err(|err| ApiError{message: err.to_string()})
}

#[get("/hello/<name>/<age>")]
pub fn hello(name: String, age: u8) -> String {
    format!("Hello, {} year old named {}!", age, name)
}

#[post("/apply", data = "<user>")]
pub fn new_user(user: String, conn_pool: State<DbPool>) -> ApiResult {
    // attempt to parse the supplied JSON into a `User` struct
    let user: NewUser = unwrap_json(&user)?;

    insert_new_user(&user, &*conn_pool.inner().get_conn())?;

    let res_message = if user.firstName == ("Test") {
        String::from("Hello, Test")
    } else {
        if let Some(middleInitial) = user.middleInitial.as_ref(){
            format!("Hello, {} {} {}.", user.firstName, middleInitial, user.lastName)
        } else {
            format!("Hello, {} {}.", user.firstName, user.lastName)
        }
    };

    ApiResult::success(res_message)
}

#[get("/get_user/<id>")]
pub fn grab_user(id: i32, conn_pool: State<DbPool>) -> Option<JSON<User>> {
    use schema::users::dsl;

    let result:Vec<User> = dsl::users
                        .filter(dsl::id.eq(id))
                        .limit(1)
                        .load(&*conn_pool.inner().get_conn())
                        .expect("Error. User Not Found");

    result.get(0).map(|user| JSON(user.clone()))
}
